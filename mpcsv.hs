{-# LANGUAGE OverloadedStrings #-}
import Control.Applicative ((<|>))
import Data.Sort (sortOn)
import Text.CSV
import Text.HTML.Scalpel
import System.IO

-- This script fetches the list of members from the Parliament API and
-- returns them in a human usable format (CSV).

-- This is the file where the CSV of MP contact data will be dumped
csvFilename = "mps.csv"

-- Various type synonyms used in Member and Address types
type DisplayName    = String
type Party          = String
type Constituency   = String
type AddrType       = String
type Addr1          = String
type Addr2          = String
type Addr3          = String
type Addr4          = String
type Addr5          = String
type TwitterHandle  = String
type FBName         = String
type InstaName      = String
type YTName         = String
type Email          = String
type Postcode       = String
type Phone          = String
type SiteURL        = String

-- A PhysicalAddress - typically a constituency or parliamentary address
data PhysicalAddress =
  PhysicalAddress Addr1 Addr2 Addr3 Addr4 Addr5 Postcode Phone Email
  deriving (Show, Eq)

-- An Address is an Address, "virtual" or physical corresponsing to
-- one of the types above. Or if a new type is added, an UnsupportedAddressType
-- Departmental Contact Form addresses, TikTok and LinkedIn get assigned to a
-- generic virtual address. Because who the fuck cares?
data Address =
  ConstituencyAddress PhysicalAddress
  | DepartmentalAddress PhysicalAddress
  | ParliamentaryAddress PhysicalAddress
  | PartyAddress PhysicalAddress
  | TwitterAddress TwitterHandle
  | FacebookAddress FBName
  | InstagramAddress InstaName
  | YouTubeAddress YTName
  | WebsiteAddress SiteURL
  | VirtualAddress AddrType Addr1
  | UnsupportedAddressType
  deriving (Show, Eq)
-- Address types were extracted from the raw XML like this:
--  $ grep -A1 'Type_Id' members.xml | tr '\n' ' ' | tr -s '\-\-' '\n' | perl -pe 's{</?Type>}{}g' | tr -s ' ' | sort | uniq 
--
--  Here's a list of the ones that existed in August 2022 - ones with a P next to them
--  are physical addresses with a building and often a phone number and email:
-- <Address Type_Id="1001"> Party P
-- <Address Type_Id="1002"> Departmental Contact Form
-- <Address Type_Id="1003"> LinkedIn
-- <Address Type_Id="1004"> TikTok
-- <Address Type_Id="1">    Parliamentary P
-- <Address Type_Id="2">    Departmental P
-- <Address Type_Id="4">    Constituency P
-- <Address Type_Id="6">    Website
-- <Address Type_Id="7">    Twitter
-- <Address Type_Id="8">    Facebook
-- <Address Type_Id="11">   Youtube
-- <Address Type_Id="12">   Instagram

-- A member is an MP. Usually they are currently serving and alive.
-- But the API allows for dead ones and ex-MPs. I make no attempt to
-- exclude any such entries.
data Member
    = Member DisplayName Party Constituency [Address]
    deriving (Show, Eq)

-- This is the URL in the parliament API where you can grab a full list of members.
membersURL :: String
membersURL = "https://data.parliament.uk/membersdataplatform/services/mnis/members/query/House=Commons/Addresses/"

-- Spit out a CSV of all the members
-- You can change the function to showAllMembersCSV if you need to print to stdout
main :: IO ()
main = fileWriteAllMemberCSV

-- Writes a CSV of all members to the file at csvFilename
fileWriteAllMemberCSV :: IO ()
fileWriteAllMemberCSV = do
  ms <- memberCSV allMembers
  case ms of
    Just members -> do
      csvFile <- openFile csvFilename WriteMode
      hPutStrLn csvFile members
      hClose csvFile
    Nothing      -> do putStrLn "Problem fetching MPs"

-- Prints a CSV of all members, or a generic error string if they can't be fetched
showAllMemberCSV :: IO ()
showAllMemberCSV = do
  ms <- memberCSV allMembers
  case ms of
    Just members -> do putStrLn members
    Nothing      -> do putStrLn "Problem fetching MPs"

-- Given a potential list of members, if the list is OK, returns
-- a CSV of their data, otherwise returns the empty string
memberCSV :: IO (Maybe [Member]) -> IO (Maybe String)
memberCSV ms = do
  let header = headerCSVRecord
  ms' <- ms
  case ms' of
    Just members -> do
      let rs = map memberCSVRecord members
      return . Just $ printCSV (header : rs)
    Nothing -> do
        return Nothing

-- Gets a list of members sorted by display name
allMembers :: IO (Maybe [Member])
allMembers = do
  ms <- scrapeURL membersURL members
  return $ fmap (sortOn showName) ms

-- Scrapes all member elements using the member scraper
members :: Scraper String [Member]
members = chroots "Member" member

-- Scrapes a single member, dealing with addresses using the addrs scraper
member :: Scraper String Member
member = do
  name <- text "DisplayAs"
  party <- text "Party"
  constituency <- text "MemberFrom"
  addresses <- chroot "Addresses" addrs
  return $ Member name party constituency addresses

-- Scrape all addresses for a member
addrs :: Scraper String [Address]
addrs = chroots "Address" addr1

-- Scrape a single physical or virtual address
addr1 :: Scraper String Address
addr1 = physicalAddress <|> virtualAddress

-- Scrape a physical address (Constituency, departmental, Parliamentary or party)
physicalAddress :: Scraper String Address
physicalAddress = do
  addrType <- text "Type"
  addr1 <- text "Address1"
  addr2 <- text "Address2"
  addr3 <- text "Address3"
  addr4 <- text "Address4"
  addr5 <- text "Address5"
  postcode <- text "Postcode"
  phone <- text "phone"
  email <- text "email"
  let pa = PhysicalAddress addr1 addr2 addr3 addr4 addr5 postcode phone email
  case addrType of
    "Constituency Office"  -> return $ ConstituencyAddress pa
    "Departmental Office"  -> return $ DepartmentalAddress pa
    "Parliamentary Office" -> return $ ParliamentaryAddress pa
    "Party Office"         -> return $ PartyAddress pa
    "Constituency office"  -> return $ ConstituencyAddress pa
    "Departmental office"  -> return $ DepartmentalAddress pa
    "Parliamentary office" -> return $ ParliamentaryAddress pa
    "Party office"         -> return $ PartyAddress pa
    _               -> return UnsupportedAddressType

-- Scrape a "virtual" address (virtual is my term not the API's)
-- These are generaly things like social media accounts run by the member
virtualAddress :: Scraper String Address
virtualAddress = do
  addrType <- text "Type"
  addr1 <- text "Address1"
  case addrType of
    "Twitter"   -> return $ TwitterAddress addr1
    "Facebook"  -> return $ FacebookAddress addr1
    "Youtube"   -> return $ YouTubeAddress addr1
    "Instagram" -> return $ InstagramAddress addr1
    "Website"   -> return $ WebsiteAddress addr1
    _           -> return $ VirtualAddress addrType addr1

-- Given a Member, extract and show the name. This is used in sorting the output.
showName :: Member -> DisplayName
showName (Member name _ _ _) = show name


-- is this a XYZ address functions. It was rather tiresome writing these, is there a better way?
isConstituencyAddress (ConstituencyAddress _) = True
isConstituencyAddress _                       = False

isDepartmentalAddress (DepartmentalAddress _) = True
isDepartmentalAddress _                       = False

isParliamentaryAddress (ParliamentaryAddress _) = True
isParliamentaryAddress _                        = False

isPartyAddress (PartyAddress _) = True
isPartyAddress _                = False

isTwitterAddress (TwitterAddress _) = True
isTwitterAddress _                  = False

isFacebookAddress (FacebookAddress _) = True
isFacebookAddress _                   = False

isInstagramAddress (InstagramAddress _) = True
isInstagramAddress _                    = False

isYouTubeAddress (YouTubeAddress _) = True
isYouTubeAddress _                  = False

isWebsiteAddress (WebsiteAddress _) = True
isWebsiteAddress _                  = False

isVirtualAddress (VirtualAddress _ _) = True
isVirtualAddress _                    = False

isUnsupportedAddress UnsupportedAddressType = True
isUnsupportedAddress _                      = False

-- This spits out the header CSV Records
headerCSVRecord :: Record
headerCSVRecord = [
    "Name"
  , "Constituency"
  , "Party"
  , "Constituency Address Line 1"
  , "Constituency Address Line 2"
  , "Constituency Address Line 3"
  , "Constituency Address Line 4"
  , "Constituency Address Line 5"
  , "Constituency Address Postcode"
  , "Constituency Phone Number"
  , "Consitituency Email"
  , "Parliamentary Address Line 1"
  , "Parliamentary Address Line 2"
  , "Parliamentary Address Line 3"
  , "Parliamentary Address Line 4"
  , "Parliamentary Address Line 5"
  , "Parliamentary Address Postcode"
  , "Parliamentary Phone Number"
  , "Parliamentary Email"
  , "Website URL"
  , "Twitter Handle"
  , "Facebook Page"
  , "YouTube Channel"
  , "Instagram Channel"
  ]

-- Given a member, return a CSV Record for that member using the data in her record.
memberCSVRecord :: Member -> Record
memberCSVRecord (Member name party constituency addresses) =
    concat [
      [ name, constituency, party]
      , constituencyA
      , parliamentaryA
      , websiteA
      , twitterA
      , facebookA
      , youTubeA
      , instagramA
      ]
  where
    getAddressMatching predicate =
      let as = filter predicate addresses
      in if null as then [""] else addressFields $ head as
    getPAddressMatching predicate =
      case getAddressMatching predicate of
        [""] -> ["","","","","","","",""]
        fs   -> fs
    constituencyA  = getPAddressMatching isConstituencyAddress
    parliamentaryA = getPAddressMatching isParliamentaryAddress
    websiteA       = getAddressMatching isWebsiteAddress
    twitterA       = getAddressMatching isTwitterAddress
    facebookA      = getAddressMatching isFacebookAddress
    youTubeA       = getAddressMatching isYouTubeAddress
    instagramA     = getAddressMatching isInstagramAddress

-- Extracts the address parts in a format appropriate for our CSV records
addressFields (ConstituencyAddress pa)  = physicalAddressFields pa
addressFields (DepartmentalAddress pa)  = physicalAddressFields pa
addressFields (ParliamentaryAddress pa) = physicalAddressFields pa
addressFields (PartyAddress        pa)  = physicalAddressFields pa
addressFields (TwitterAddress handle)   = [handle]
addressFields (FacebookAddress name)    = [name]
addressFields (InstagramAddress name)   = [name]
addressFields (YouTubeAddress name)     = [name]
addressFields (WebsiteAddress url)      = [url]
addressFields (VirtualAddress addType add1)
                                     = [ show addType
                                       , ","
                                       , show add1
                                       ]
addressFields UnsupportedAddressType = []

-- Given a physical address return the address components in the right format for
-- address fields.
physicalAddressFields (PhysicalAddress addr1 addr2 addr3 addr4 addr5 postcode phone email) =
  [addr1, addr2, addr3, addr4, addr5, postcode, phone, email]
