# mpcsv: Fetch an actually useful list of MP's details from the Parliament API

Most humans struggle to read the XML and JSON that the UK Parliament provide in their API. It is a failing in our species.

Some time ago I collaborated on a nice project to rehumanise the data from this API - over at https://github.com/UKParlDataSheets.

Fast forward a few years and that project seems to have gone away.

So this is a remake in Haskell. Because I like to write Haskell.

mpcsv is designed to run as a cron job, and to fetch a CSV of MPs containing: name, constituency, party, parliamentary and
constituency addresses, and their social media details if they are present. By default it writes a file called mps.csv. Edit the
CSVFilepath as required.

You can view the output in this repository or a daily updated one at: https://static.charlieharvey.org.uk/files/mps.csv

Incidentally I don't know why the API returns London in the second field for some Parliamentary addresses, and the fifth field 
in others. I imagine it is something to do with Brexit.
